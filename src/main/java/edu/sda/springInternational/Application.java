package edu.sda.springInternational;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {

        //http://localhost:8080/get-greeting?language=ro

        SpringApplication.run(Application.class, args);
    }
}
